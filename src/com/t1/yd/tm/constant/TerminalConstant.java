package com.t1.yd.tm.constant;

public class TerminalConstant {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";

}
